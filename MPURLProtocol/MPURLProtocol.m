

#import "MPURLProtocol.h"

static NSString * const URLProtocolHandledKey = @"URLProtocolHandledKey";

@interface MPURLProtocol ()<NSURLConnectionDelegate>
@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionDataTask *task;
@end

@implementation MPURLProtocol

NSDictionary *connectionProxyDictionary;
NSString *redirectHost;

+ (void)proxyHTTP:(NSString *)host port:(NSUInteger)port {
    connectionProxyDictionary = @{
                                  @"HTTPEnable":@YES,
                                  (id)kCFStreamPropertyHTTPProxyHost:host,
                                  (id)kCFStreamPropertyHTTPProxyPort:@(port),
                                  };
}
/** HTTPS contain HTTP */
+ (void)proxyHTTPS:(NSString *)host port:(NSUInteger)port {
    connectionProxyDictionary = @{
                                  @"HTTPEnable":@YES,
                                  (id)kCFStreamPropertyHTTPProxyHost:host,
                                  (id)kCFStreamPropertyHTTPProxyPort:@(port),
                                  @"HTTPSEnable":@YES,
                                  (id)kCFStreamPropertyHTTPSProxyHost:host,
                                  (id)kCFStreamPropertyHTTPSProxyPort:@(port),
                                  };
}

+ (void)proxySOCKS:(NSString *)host port:(NSUInteger)port {
    connectionProxyDictionary = @
    {
        @"SOCKSEnable":@YES,
        (id)kCFStreamPropertySOCKSProxyHost: host,
        (id)kCFStreamPropertySOCKSProxyPort: @(port)
    };
}

+ (void)redirectHost:(NSString *)host {
    redirectHost = host;
}

+ (void)proxyStart {
    [NSURLProtocol registerClass:[self class]];
}

+ (void)proxyStop {
    [NSURLProtocol unregisterClass:[self class]];
}

+ (BOOL)canInitWithRequest:(NSURLRequest *)request
{
    //只处理http和https请求
    NSString *scheme = [[request URL] scheme];
    if ( ([scheme caseInsensitiveCompare:@"http"] == NSOrderedSame ||
          [scheme caseInsensitiveCompare:@"https"] == NSOrderedSame))
    {
        //看看是否已经处理过了，防止无限循环
        if ([NSURLProtocol propertyForKey:URLProtocolHandledKey inRequest:request]) {
            return NO;
        }
        
        return YES;
    }
    return NO;
}

+ (NSURLRequest *) canonicalRequestForRequest:(NSURLRequest *)request {
    NSMutableURLRequest *mutableReqeust = [request mutableCopy];
    if (redirectHost) {
        mutableReqeust = [self redirectHostInRequset:mutableReqeust];
    }
    return mutableReqeust;
}

+ (BOOL)requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b
{
    return [super requestIsCacheEquivalent:a toRequest:b];
}

- (void)startLoading
{
    /* 如果想直接返回缓存的结果，构建一个NSURLResponse对象
     if (cachedResponse) {
     
     NSData *data = cachedResponse.data; //缓存的数据
     NSString *mimeType = cachedResponse.mimeType;
     NSString *encoding = cachedResponse.encoding;
     
     NSURLResponse *response = [[NSURLResponse alloc] initWithURL:self.request.URL
     MIMEType:mimeType
     expectedContentLength:data.length
     textEncodingName:encoding];
     
     [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
     [self.client URLProtocol:self didLoadData:data];
     [self.client URLProtocolDidFinishLoading:self];
     */
    
    NSMutableURLRequest *mutableReqeust = [[self request] mutableCopy];
    
    //打标签，防止无限循环
    [NSURLProtocol setProperty:@YES forKey:URLProtocolHandledKey inRequest:mutableReqeust];
    
    self.connection = [NSURLConnection connectionWithRequest:mutableReqeust delegate:self];
    
    if (connectionProxyDictionary) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        configuration.connectionProxyDictionary = connectionProxyDictionary;
        
        self.session = [NSURLSession sessionWithConfiguration:configuration];
        self.task = [self.session dataTaskWithRequest:self.request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error) {
                [self.client URLProtocol:self didFailWithError:error];
            } else {
                [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageAllowed];
                [self.client URLProtocol:self didLoadData:data];
                [self.client URLProtocolDidFinishLoading:self];
            }
        }];
        [self.task resume];
    }
}

- (void)stopLoading
{
    [self.connection cancel];
}

#pragma mark - NSURLConnectionDelegate

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.client URLProtocol:self didLoadData:data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.client URLProtocolDidFinishLoading:self];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.client URLProtocol:self didFailWithError:error];
}

#pragma mark -- private

+ (NSMutableURLRequest*)redirectHostInRequset:(NSMutableURLRequest*)request
{
    if ([request.URL host].length == 0) {
        return request;
    }
    
    NSString *originUrlString = [request.URL absoluteString];
    NSString *originHostString = [request.URL host];
    NSRange hostRange = [originUrlString rangeOfString:originHostString];
    if (hostRange.location == NSNotFound) {
        return request;
    }

    // 替换host
    NSString *urlString = [originUrlString stringByReplacingCharactersInRange:hostRange withString:redirectHost];
    NSURL *url = [NSURL URLWithString:urlString];
    request.URL = url;
    
    return request;
}


@end
