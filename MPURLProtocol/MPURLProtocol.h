

#import <Foundation/Foundation.h>

@interface MPURLProtocol : NSURLProtocol

+ (void)proxyHTTP:(NSString *)host port:(NSUInteger)port;
/** HTTPS contain HTTP */
+ (void)proxyHTTPS:(NSString *)host port:(NSUInteger)port;

+ (void)proxySOCKS:(NSString *)host port:(NSUInteger)port;
/** 重定向HOST */
+ (void)redirectHost:(NSString *)host;
/** 先设置上面的方法，最后在AppDelegate.h调用此方法，注意顺序*/
+ (void)proxyStart;

+ (void)proxyStop;

@end
