//
//  ViewController.m
//  MPURLProtocolExample
//
//  Created by mopellet on 2017/6/26.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import "ViewController.h"
#import "MPURLProtocol.h"
@interface ViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UIButton *go;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // 1.配置信息 (HTTP、HTTPS、SOCKS、Redirect)
//    [MPURLProtocol proxyHTTP:@"192.168.0.12" port:8888];
    [MPURLProtocol redirectHost:@"cn.bing.com"];
    // 2.注册代理
    [MPURLProtocol proxyStart];
    // 3.移除代理
//    [MPURLProtocol proxyStop];
}

- (IBAction)go:(id)sender {
    if ([self.urlTextField isFirstResponder]) {
        [self.urlTextField resignFirstResponder];
    }
    
    [self sendRequest];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    [self sendRequest];
    
    return YES;
}

#pragma mark - request

- (void) sendRequest {
    
    NSString *text = self.urlTextField.text;
    if (![text isEqualToString:@""]) {

        NSURL *url = [NSURL URLWithString:text];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
        
    }
    
}

@end
