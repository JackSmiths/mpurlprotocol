# MPURLProtocol
Custom NSURLProtocol 三步启动


## 描述
* 支持HTTP端口代理
* 支持HTTPS端口代理
* 支持SOCKS端口代理
* 支持HOST重定向

## 用法

```objc
    
    // 1.配置信息 (HTTP、HTTPS、SOCKS、Redirect)
    [MPURLProtocol proxyHTTP:@"192.168.0.12" port:8888];
    //[MPURLProtocol redirectHost:@"cn.bing.com"];
    // 2.注册代理
    [MPURLProtocol proxyStart];
    // 3.移除代理
    [MPURLProtocol proxyStop];
}
```

## 联系方式:
* QQ : 351420450
* Email : mopellet@foxmail.com
* 更多相关问题欢迎加入QQ交流群（群号：92073722）![](https://raw.githubusercontent.com/MoPellet/MPVPNManager/master/qqgroup.png)

## 特别鸣谢:
* 您的star的我开发的动力，如果代码对您有帮助麻烦动动您的小手指点个start。
